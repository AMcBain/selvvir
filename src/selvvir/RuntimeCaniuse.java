package selvvir;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zwitserloot.json.JSON;

/*
 * Care should be taken as this class throws RuntimeExceptions not Exceptions, given that code relying
 * on instances of this class doesn't expect the file it is watching to become unavailable at runtime.
 */
public class RuntimeCaniuse {

	private final File file;
	private Long last;

	private Set<String> mobiles;

	private Map<String, String> statuses;

	private Map<String, String> agents;
	private Map<String, JSON> usages;
	private Map<String, String> future;
	private Map<String, String> names;

	private Map<String, JSON> features;
	private Map<String, String> fkeys;

	public RuntimeCaniuse(String path) {
		this(new File(path));
	}

	public RuntimeCaniuse(File file) {
		this.file = file;
		load();

		mobiles = new HashSet<String>();
		mobiles.add("ios_saf");
		mobiles.add("op_mini");
		mobiles.add("android");
		mobiles.add("bb");
		mobiles.add("op_mob");
		mobiles.add("and_chr");
		mobiles.add("and_ff");
		mobiles.add("ie_mob");
	}

	private void load() {
		try {
			BufferedReader stream = null;
			try {
				stream = new BufferedReader(new FileReader(file));

				StringBuilder builder = new StringBuilder();
				String line;
				while ((line = stream.readLine()) != null) {
					builder.append(line);
				}

				JSON raw = JSON.parse(builder.toString());

				statuses = new HashMap<String, String>();
				usages = new HashMap<String, JSON>();
				agents = new HashMap<String, String>();
				future = new HashMap<String, String>();
				names = new HashMap<String, String>();
				features = new HashMap<String, JSON>();
				fkeys = new HashMap<String, String>();

				JSON json = raw.get("statuses");
				Set<String> keys = json.keySet();

				for (String key : keys) {
					statuses.put(key, json.get(key).asString());
				}

				json = raw.get("agents");
				keys = json.keySet();

				for (String key : keys) {
					JSON entry = json.get(key);

					List<JSON> versions = entry.get("versions").asList();
					agents.put(key, versions.get(versions.size() - 4).asString());
					future.put(key, versions.get(versions.size() - 3).asString());

					names.put(key, entry.get("browser").asString());
					usages.put(key, entry.get("usage_global"));
				}

				json = raw.get("data");
				keys = json.keySet();

				for (String key : keys) {
					JSON entry = json.get(key);

					String title = entry.get("title").asString();
					String desc = entry.get("description").asString();
					String categories = "";

					for (JSON cat : entry.get("categories").asList()) {

						categories += " " + cat.asString();
					}

					String map = (key + " " + title + " " + desc + categories).toLowerCase();
					features.put(map, entry);
					fkeys.put(map, key);
				}

				last = file.lastModified();
			}
			finally {
				if (stream != null) {
					stream.close();
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("couldn't reload properties file", e);
		}
	}

	public List<Feature> find(String text, boolean desktop, boolean mobile, boolean near, boolean partialAsYes, boolean prefixedAsNo) {
		text = text.toLowerCase();

		if (!desktop && !mobile) {
			return Collections.emptyList();
		}

		synchronized (file) {
			if (file.lastModified() != last) {
				load();
			}
		}

		List<Feature> results = new ArrayList<Feature>();
		for (String key : features.keySet()) {

			if (key.indexOf(text) != -1) {
				JSON feature = features.get(key);

				StringBuilder builder = new StringBuilder();
				try {
					builder.append("http://caniuse.com/#feat=" + URLEncoder.encode(fkeys.get(key), "utf-8"));
				} catch (UnsupportedEncodingException e) {
					builder.append("caniuse.com");
				}
				builder.append(" • ");
				builder.append(feature.get("title").asString());
				builder.append(" • ");
				builder.append(statuses.get(feature.get("status").asString()));
				builder.append(" • Global support: ");

				double usage = 0;
				List<String> yes = new ArrayList<String>();
				List<String> no = new ArrayList<String>();

				JSON stats = feature.get("stats");
				for (String agent : stats.keySet()) {

					JSON version = stats.get(agent).get(agents.get(agent));
					JSON next = stats.get(agent).get(future.get(agent));

					if (near && next.exists() && !next.isNull()) {
						version = next;
					}

					if ((mobiles.contains(agent) && mobile) || (!mobiles.contains(agent) && desktop)) {

						String verStr = version.asString();
						boolean isYes = (verStr.startsWith("y") || (verStr.startsWith("a") && partialAsYes));

						if (isYes && (!prefixedAsNo || !verStr.endsWith("x"))) {
							yes.add(names.get(agent));
						}
						else {
							no.add(names.get(agent));
						}

						JSON versions = stats.get(agent);
						for (String entry : versions.keySet()) {

							verStr = versions.get(entry).asString();
							isYes = (verStr.startsWith("y") || (verStr.startsWith("a") && partialAsYes));

							if (isYes && (!prefixedAsNo || !verStr.endsWith("x"))) {
								usage += usages.get(agent).get(entry).asDouble();
							}
						}
					}
				}

				//builder.append(feature.get("usage_perc_y").asString());
				builder.append(String.format("%1.2f", usage));
				builder.append("% • ");

				String type = "fully";
				if (partialAsYes) {
					type = "partially or fully";
				}

				// Title. Status. Global support: X%. All support it fully but X, and Z.
				// Title. Status. Global support: X%. Only X, and Z fully support it.
				if (yes.size() > no.size() && no.size() > 0) {

					if (near) {
						builder.append("All current and upcoming browsers support this " + type + " except ");
					}
					else {
						builder.append("All browsers currently support this " + type + " except ");
					}

					for (int i = 0; i < no.size(); i++) {

						builder.append(no.get(i));

						if (i < no.size() - 1) {
							builder.append(", ");
						}
						if (i == no.size() - 2) {
							builder.append("and ");
						}
					}
					builder.append(".");
				}
				else if (no.size() > yes.size() && yes.size() > 0) {
					builder.append("Only ");

					for (int i = 0; i < yes.size(); i++) {

						builder.append(yes.get(i));

						if (i < yes.size() - 1) {
							builder.append(", ");
						}
						if (i == yes.size() - 2) {
							builder.append("and ");
						}
					}

					builder.append(" support");
					if (yes.size() == 1) {
						builder.append("s");
					}
					builder.append(" this fully.");
				}
				else if (yes.isEmpty()) {
					if (near) {
						builder.append("No current or upcoming browsers support this.");
					}
					else {
						builder.append("No browsers currently support this.");
					}
				}
				else {
					if (near) {
						builder.append("All current and upcoming browsers support this " + type + ".");
					}
					else {
						builder.append("All browsers currently support this " + type + ".");
					}
				}

				String spec = null;
				if (feature.get("spec").exists()) {

					spec = feature.get("spec").asString();
				}

				String link = null;
				if (feature.get("links").asList().size() > 0) {

					link = feature.get("links").asList().get(0).get("url").asString();
				}

				results.add(new Feature(fkeys.get(key), builder.toString(), spec, link));
			}
		}

		return results;
	}

	public static class Feature {

		public final String key;
		public final String value;
		public final String specUrl;
		public final String firstUrl;

		public Feature(String key, String value, String spec, String first) {
			this.key = key;
			this.value = value;
			specUrl = spec;
			this.firstUrl = first;
		}
	}

}
