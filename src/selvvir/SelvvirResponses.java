package selvvir;

public enum SelvvirResponses {

	NOT_FOUND("no such command '%s'. Try " + Selvvir.PREFIX + "search <query> to search for factoids or see " + Selvvir.PREFIX + "commands"), //

	BROKEN_ALIAS("« %s » is an alias leading to a non-existent factoid « %s »; please contact an administrator (see « " + Selvvir.PREFIX
			+ "admins ») so they can clean it up :)"), //

	MISSING_ARGUMENTS("Sorry, it looks like you're missing some arguments!"), //

	ADMIN_ONLY("Sorry, you have to be an admin to use this command"), //

	ENCODING_ERROR("%s, Expectation failed. UTF-8 encoding does not exist."), //

	COMMAND_INFO("%s • %s"), //
	COMMAND_INFO_ADMIN("%s • %s • admin only"), //
	COMMAND_SYNTAX("Syntax: %s[/%s] %s• %s"), //
	COMMAND_SYNTAX_NO_FLAGS("Syntax: %s %s• %s"), //
	COMMAND_FLAGS("--- This command supports the following flags ---"), //
	COMMAND_FLAG("%s • %s"), //
	COMMAND_NOTE("Note: %s"), //

	INFO_NO_FACTOID_NAME("Sorry, I didn't see a factoid name there!"), //
	INFO_NO_FACTOID_VALUE("Sorry, I couldn't find a factoid with the name « %s »"), //

	FACTOID_IS_ALIAS("« %s » is an alias for « %s »"), //
	FACTOID_NOT_ALIAS("« %s » is not an alias"), //
	FACTOID_ALIASES("The following names are aliases for « %s »: %s"), //
	FACTOID_NO_ALIASES("There are no aliases for « %s »"), //
	FACTOID_INFO("« %s » is « %s »"), //

	FACTOID_EXISTS("Sorry, a factoid with the name « %s » already exists. If you want to overwrite this factoid, you must use the '!' flag"), //
	FACTOID_ADDED("I successfully added the factoid « %s »"), //
	FACTOID_UPDATED("I successfully updated the factoid « %s »"), //
	FACTOID_DELETED("I successfully deleted factoid « %s » which had the content « %s »"), //
	FACTOID_NOT_EXISTS("Sorry, you can't delete a factoid that doesn't exist!"), //
	FACTOID_NO_ALIAS("Sorry, but you can't create factoids of which the content starts with 'alias:'"), //
	// Not a response from rivvles, captures the case where saving the Preferences used (Java) fails.
	FACTOID_FAILED("I failed to %s factoid « %s »"), //

	// Changed JavaScript to Java here.
	REGEX_INVALID_FMT(
			"Sorry, that's an invalid regexp argument; the expected format is `<factoid-name> = s/<find>/<replace>/<flags>` where <find> is a Java-compatible regular expression"), //
	// Not a response from rivvles, made to capture the case it didn't handle.
	REGEX_INVALID("Sorry, that's an invalid regexp; check your syntax"), //
	REGEX_NO_TARGET("Sorry, but I couldn't find a factoid with the name « %s »"), //
	REGEX_UPDATE("I successfully updated the factoid « %s » with content « %s »"), //

	ALIAS_ADDED("I successfully created « %s » as an alias for « %s »"), //
	ALIAS_DELETED("I successfully deleted factoid « %s » which was an alias for « %s »"), //
	ALIAS_DELETED_ALL("I successfully deleted the factoid « %s » and all aliases leading to it. The deleted factoid's content was « %s »"), //
	ALIAS_CIRCULAR("Sorry, I won't let you create a circular reference :)"), //
	ALIAS_TARGET_NO_EXISTS("Sorry, I couldn't find an existing factoid with the name « %s » to create an alias for"), //
	ALIAS_REFERENCED("I noticed the factoid you're trying to delete has aliases leading to it (see « " + Selvvir.PREFIX
			+ "info %s »). If you want to delete this factoid, and all aliases leading to it, you must specify the '!' flag"), //

	SEARCH_NO_TERM("Sorry, I didn't see a search term there!"), //
	SEARCH_NO_RESULTS("Sorry, I couldn't find any results for query « %s »"), //
	SEARCH_RESULTS("Found %s results for query « %s »: %s"), //
	SEARCH_RESULTS_MANY("Found %s results for query « %s » (showing %s): %s"), //

	VALIDATION_NO_URL("No URL provided"), //
	VALIDATION_MARKUP_ERROR("Oops, something went wrong trying to validate the markup for « %s »"), //
	VALIDATION_CSS_ERROR("Oops, something went wrong trying to validate the styles for « %s »"), //
	VALIDATION_MARKUP("%s, « %s » markup • errors: %s • warnings: %s • charset: %s • validation result: %s"), //
	VALIDATION_CSS("%s, « %s » CSS • errors: %s • warnings: %s • profile: %s • validation result: %s"), //

	GOOGLE_NO_TEXT("No text provided"), //
	GOOGLE_ERROR("Oops, something went wrong!"), //
	GOOGLE_NO_RESULTS("No results for query '%s'"), //
	GOOGLE_RESULT("%s, Google says \"%s\" • %s • More results: %s"), //

	BING_NO_TEXT("No text provided"), //
	BING_ERROR("Sorry, \"Just bing it!\" didn't fly."), //
	BING_NO_RESULTS("No results for query '%s'"), //
	BING_RESULT("%s, Bing says \"%s\" • %s • More results: %s"), //

	ADMINS_NONE("I have no admins"), //
	ADMINS("%s"), //

	GOOGLE_SHORT_NO_URL("No URL provided"), //
	GOOGLE_SHORT_ERROR("Oops, something went wrong trying to shorten « %s »"), //
	GOOGLE_SHORT("%s, Short URL for « %s » is « %s »"), //

	CANIUSE_NO_DATA("No caniuse data loaded"), //
	CANIUSE_NO_TEXT("No text provided"), //
	CANIUSE_NO_RESULTS("Sorry, I couldn't find any features for query « %s »"), //
	CANIUSE_RESULTS("Found %s features for query « %s »: %s"), //
	CANIUSE_RESULTS_MANY("Found %s features for query « %s » (showing %s): %s"), //
	CANIUSE("%s, %s");

	private final String value;

	private SelvvirResponses(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
