package selvvir;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.WeakHashMap;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

import com.zwitserloot.json.JSON;

import selvvir.AbstractSelvvirCommand.SelvvirCommandTerminated;

// suggestions: statcounter browser use rates, would be good additions to the bot
public class Selvvir extends PircBot {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String server = System.getProperty("server", "wolfe.freenode.net");
		String channels = System.getProperty("channel", "#pircbot");
		String key = System.getProperty("KEY", "");
                String capability = System.getProperty("CAP", "identify-msg");
		String name = System.getProperty("name", "Selvvir");
		String password = System.getProperty("password", null);
		String verbose = System.getProperty("verbose", "");
		String auth = System.getProperty("auth", "authenticated.properties");
		String bitlylogin = System.getProperty("bitlylogin", null);
		String bitlykey = System.getProperty("bitlykey", null);
		String binguser = System.getProperty("binguser", null);
		String bingkey = System.getProperty("bingkey", null);

		new Selvvir(server, channels.split(","), key, capability, name, password, "verbose".equals(verbose), auth, bitlylogin, bitlykey, binguser, bingkey);
	}

	public static final String ALIAS_PREFIX = "alias:";

	private static final String DIRTY_KEY = "dirty";
	private static final String INDEX_KEY = "index";
	private static final String FACTOIDS_KEY = "factoids";
	private static final String STATISTICS_KEY = "statistics";

	private static final Pattern URL_REGEX = Pattern
			.compile("(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))");

	protected static final String PREFIX = "`";

	private final String originalName;

	private final String pkey;
	private final boolean capabilityIdentifyMsg;
	private final boolean capabilityAccountTag;

	private final Map<String, AbstractSelvvirCommand> commands;
	private final Preferences factoids;
	private final Preferences statistics;
	private JSON index;

	private RuntimeProperties auth;

	private String lastUrl;

	private Field jsonObjectField;

	private boolean indexSynched;

	private Map<String, Void> talkedToTheBot;
	private List<String> eightBallAnswers;
	private Random eightBall;

	private Timer aggressiveTakeBackTimer;
	private String accountPassword;
	private String bitlylogin;
	private String bitlykey;
	private String binguser;
	private String bingkey;

	private String lastMessage;
	private long lastMessageTime;

	public Selvvir(String server, String[] channels, String key, String capability, String name, String password, boolean verbose, String authPath, String bitlylogin, String bitlykey, String binguser, String bingkey) {
		commands = new LinkedHashMap<String, AbstractSelvvirCommand>();

		if (key != null && !key.isEmpty()) {
			pkey = "-" + key;
		}
		else {
			pkey = "";
		}
		capabilityIdentifyMsg = "identify-msg".equals(capability);
		capabilityAccountTag = "account-tag".equals(capability);

		statistics = Preferences.userNodeForPackage(getClass()).node(STATISTICS_KEY + pkey);
		factoids = Preferences.userNodeForPackage(getClass()).node(FACTOIDS_KEY + pkey);
		indexFactoidAliases();

		ServiceLoader<AbstractSelvvirCommand> loader = ServiceLoader.load(AbstractSelvvirCommand.class);
		Iterator<AbstractSelvvirCommand> itr = loader.iterator();

		try {
			if (capabilityIdentifyMsg || capabilityAccountTag) {
				auth = new RuntimeProperties(authPath);
			}
			else {
				System.err.println("Admin commands will not be available.");
			}
		}
		catch (RuntimeException e) {
			e.printStackTrace();
			System.err.println("Admin commands will not be available.");
		}

		this.bitlylogin = bitlylogin;
		this.bitlykey = bitlykey;
		this.binguser = binguser;
		this.bingkey = bingkey;

		while (itr.hasNext()) {
			AbstractSelvvirCommand command = itr.next();
			commands.put(command.name(), command);
		}

		try {
			// As long as the library doesn't change, we're good!
			jsonObjectField = JSON.class.getDeclaredField("object");
			jsonObjectField.setAccessible(true);

			setVerbose(verbose);
			setName(name);
			setLogin("copy");
			setAutoNickChange(true);
			setEncoding("UTF-8");

			if (!server.isEmpty()) {

				if (password == null) {
					connect(server, 6697);
				}
				else {
					connect(server, 6697, password);
				}

				for (int i = 0; i < channels.length; i++) {
					joinChannel(channels[i]);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		originalName = name;
		talkedToTheBot = new WeakHashMap<String, Void>();

		eightBallAnswers = new ArrayList<String>();
		eightBallAnswers.add("It is certain.");
		eightBallAnswers.add("Make it so.");
		eightBallAnswers.add("Without a doubt.");
		eightBallAnswers.add("Yes definitely.");
		eightBallAnswers.add("You may rely on it.");
		eightBallAnswers.add("As I see it, yes.");
		eightBallAnswers.add("Most likely.");
		eightBallAnswers.add("Outlook good.");
		eightBallAnswers.add("Yes.");
		eightBallAnswers.add("Signs point to yes.");
		eightBallAnswers.add("Reply hazy try again.");
		eightBallAnswers.add("Ask again later.");
		eightBallAnswers.add("Better not tell you now.");
		eightBallAnswers.add("Cannot predict now.");
		eightBallAnswers.add("Concentrate and ask again.");
		eightBallAnswers.add("Don't count on it.");
		eightBallAnswers.add("My reply is no.");
		eightBallAnswers.add("My sources say no.");
		eightBallAnswers.add("Outlook not so good.");
		eightBallAnswers.add("Very doubtful.");
		eightBall = new Random();

		aggressiveTakeBackTimer = new Timer(true);
		accountPassword = password;
	}

	private void indexFactoidAliases() {
		Preferences parent = factoids.parent();

		boolean dirty = parent.getBoolean(DIRTY_KEY + pkey, true);
		try {

			// Attempt to set dirty for any changes made while running.
			parent.putBoolean(DIRTY_KEY + pkey, true);
			parent.flush();

			if (!dirty) {

				index = JSON.parse(factoids.parent().get(INDEX_KEY + pkey, "{}"));
				indexSynched = true;
				return;
			}
			index = JSON.newMap();

			String[] factoids = getFactoids();
			if (factoids == null) {

				System.err.println("Building alias index failed!");
				return;
			}

			for (String from : factoids) {
				String to = getFactoid(from);

				if (to.startsWith("alias:")) {

					index.get(getAliasTarget(to)).add().setString(from);
				}
			}
			saveIndex(true);
		}
		catch (BackingStoreException e) {
			//
		}
	}

	private void saveIndex(boolean valid) {

		synchronized (index) {
			try {
				Preferences parent = factoids.parent();
				parent.put(INDEX_KEY + pkey, index.toJSON());

				if (valid) {
					parent.putBoolean(DIRTY_KEY + pkey, false);
				}
				parent.flush();

				if (valid) {
					indexSynched = true;
				}
			}
			catch (BackingStoreException e) {

				System.err.println("Unable to save index!");
				e.printStackTrace();
			}
		}
	}

	public String getBitlyLogin() {
		return bitlylogin;
	}

	public String getBitlyKey() {
		return bitlykey;
	}

	public String getBingUser() {
		return binguser;
	}

	public String getBingKey() {
		return bingkey;
	}

	public Iterable<AbstractSelvvirCommand> commands() {
		return commands.values();
	}

	public String getAliasTarget(String alias) {
		return alias.substring(ALIAS_PREFIX.length());
	}

	public List<JSON> getAliases(String factoid) {
		synchronized (index) {
			return index.get(factoid).asList();
		}
	}

	private String getFactoid(String key, String def) {
		return factoids.get(key, factoids.get(def, null));
	}

	public String getFactoid(String key) {
		return factoids.get(key, null);
	}

	// If this fails with an error, the index will be out of date and when the bot closes down it won't know to fix it on next start.
	private void updateAliasIndex(String factoid, String value) throws Exception {
		boolean synched = indexSynched;

		if (value != null && value.startsWith(ALIAS_PREFIX)) {
			indexSynched = false;

			// The following code is definitely dangerous.
			String target = getAliasTarget(value);

			@SuppressWarnings("unchecked")
			Map<?, Object> object = (Map<?, Object>) jsonObjectField.get(index.get(target));

			@SuppressWarnings("unchecked")
			List<String> list = (List<String>) object.get(target);

			if (list != null) {
				Iterator<String> iterator = list.iterator();

				while (iterator.hasNext()) {

					if (factoid.equals(iterator.next())) {
						iterator.remove();
					}
				}
			}

			if (synched) {
				indexSynched = true;
			}
		}
	}

	public boolean setFactoid(String key, String value) {
		boolean success = false;

		synchronized (index) {
			try {
				String oldValue = factoids.get(key, null);

				factoids.put(key, value);
				factoids.flush();
				success = true;

				updateAliasIndex(key, oldValue);

				if (value.startsWith(ALIAS_PREFIX)) {

					index.get(getAliasTarget(value)).add().setString(key);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	public boolean deleteFactoid(String factoid) {
		boolean success = false;

		synchronized (index) {
			try {
				String value = factoids.get(factoid, null);

				factoids.remove(factoid);
				factoids.flush();
				success = true;

				updateAliasIndex(factoid, value);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	public String[] getFactoids() {
		String[] keys = null;
		try {
			keys = factoids.keys();
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
		}
		return keys;
	}

	private final void findURL(String message) {
		Matcher matches = URL_REGEX.matcher(message);

		while (matches.find()) {
			lastUrl = matches.group();
		}
	}

	public String getLastUrl() {
		return lastUrl;
	}

	public Set<Object> getAuthenticated() {

		if (auth != null) {
			return auth.getKeys();
		}
		return null;
	}

	public boolean isAuthenticated(String account, String username) {
		if (auth != null) {
			return capabilityIdentifyMsg ? auth.getBoolean(username) : auth.getBoolean(account);
		}
		return false;
	}

	public void sendMessage(String channel, SelvvirResponses response, String... values) {

		sendMessage(channel, String.format(response.toString(), (Object[]) values));
	}

	public void sendNotice(String channel, SelvvirResponses response, String... values) {

		sendNotice(channel, String.format(response.toString(), (Object[]) values));
	}

	@Override
	protected void onConnect() {
		if (capabilityIdentifyMsg) {
			// Important! This prefixes all messages with + (for being logged in to an account) or - if not.
			sendRawLineViaQueue("CAP REQ identify-msg");
			sendRawLineViaQueue("CAP END");
		}
		else if (capabilityAccountTag) {
			sendRawLineViaQueue("CAP REQ account-tag");
			sendRawLineViaQueue("CAP END");
		}
	}

	@Override
	protected void onUserList(String channel, User[] users) {

		if (!originalName.equals(getNick())) {
			boolean found = false;

			for (User user : users) {
				if (originalName.equals(user.getNick())) {
					found = true;
					break;
				}
			}

			if (!found) {
				changeNick(originalName);
			}
		}
	}

	@Override
	protected void onMessage(String prefix, String channel, String sender, String login, String hostname, String message) {

		if (capabilityAccountTag) {
			return;
		}
		handleMessage(prefix, channel, null, sender, login, hostname, message, false);
	}

	@Override
	protected void onMessage(String prefix, String channel, String account, String sender, String login, String hostname, String message) {

		handleMessage(prefix, channel, account, sender, login, hostname, message, false);
	}

	@Override
	protected void onPrivateMessage(String sender, String login, String hostname, String message) {

		if (capabilityAccountTag) {
			return;
		}
		onPrivateMessage(null, sender, login, hostname, message);
	}

	@Override
	protected void onPrivateMessage(String account, String sender, String login, String hostname, String message) {

		String prefix = capabilityIdentifyMsg ? "+" : "";

		if (message.equals(prefix + "``exit hyperspace") && isAuthenticated(account, sender)) {

			quitServer("bye!");
		}
		else {
			handleMessage(null, sender, account, sender, login, hostname, message, true);
		}

	}

	private void handleMessage(String prefix, String channel, String account, String sender, String login, String hostname, String message, boolean privateMsg) {

		long now = new Date().getTime();
		boolean identified = false;

		if (capabilityIdentifyMsg) {
			identified = message.startsWith("+");
			message = message.substring(1);
		}
		else if (capabilityAccountTag) {
			identified = account != null;
		}

		if (message.equals("!l") || message.equals("!list")) {
			message = PREFIX + message;
		}

		if (message.equals("! status") || message.equals("!status")) {
			message = PREFIX + message;
		}

		if (message.startsWith("!chk")) {
			message = PREFIX + "!chk";
		}

		if (privateMsg && !message.startsWith(PREFIX)) {
			message = PREFIX + message;
		}

		if (message.startsWith(PREFIX)) {
			message = message.substring(PREFIX.length()).trim();

			if (message.isEmpty()) {
				return; // no reply
			}
			else if (message.equals(lastMessage) && now - lastMessageTime <= 15000) {
				return; // anti-spam
			}

			lastMessage = message;
			lastMessageTime = now;

			String target = sender;

			if (message.contains(" @ ")) {
				int at = message.indexOf(" @ ");
				String after = message.substring(at + 3);
				message = message.substring(0, at).trim();

				if (!after.isEmpty()) {
					target = after.trim();
				}
			}

			String command = message;
			String flags = "";
			int space = message.indexOf(' ');

			if (space != -1) {
				command = message.substring(0, space);
			}

			int slash = command.indexOf('/');
			if (slash != -1) {

				flags = command.substring(slash + 1);
				command = command.substring(0, slash);
			}

			AbstractSelvvirCommand cmd = commands.get(command);
			if (cmd != null) {
				message = (space == -1 ? "" : message.substring(space + 1)).trim();

				if (!cmd.isAdminOnly() || (isAuthenticated(account, sender) && identified)) {

					runCommand(cmd, flags, channel, account, sender, target, message);
				}
				else {

					sendNotice(sender, SelvvirResponses.ADMIN_ONLY);
				}
			}
			else {
				// Not a command so it doesn't need extra text like "`get "
				String value = getFactoid(message);

				if (value == null) {
					if (message.endsWith("?")) {
						sendMessage(channel, target + ", " +
								eightBallAnswers.get(eightBall.nextInt(eightBallAnswers.size())));
					}
					else {
						sendNotice(sender, SelvvirResponses.NOT_FOUND, message);
					}
				}
				else {
					String key = value;

					if (value.startsWith("alias:")) {
						value = getAliasTarget(value);
						statistics.putInt(value, statistics.getInt(value, 0) + 1);
						value = getFactoid(value);
					}

					if (value == null) {
						sendNotice(sender, SelvvirResponses.BROKEN_ALIAS, message, key.split(":")[1]);
					}
					else {
						value = value.replaceAll("\\{target\\}", target);
						sendMessage(channel, target + ", " + value);
					}

					statistics.putInt(message, statistics.getInt(message, 0) + 1);
					try {
						statistics.flush();
					}
					catch (BackingStoreException e) {
						e.printStackTrace();
					}
				}
			}
		}
		else {
			findURL(message);
		}

		if (message.startsWith(getNick()) && !talkedToTheBot.containsKey(sender)) {

			String res = getFactoid("autobot", "bot");
			if (res != null) {

				// talkedToTheBot.put(sender, null);
				// sendMessage(channel, sender + ", " + res);
			}
		}
	}

	private void runCommand(AbstractSelvvirCommand cmd, String flags, String channel, String account, String sender, String target, String message) {

		try {
			cmd.run(this, flags, channel, account, sender, target, message);
		}
		catch (SelvvirCommandTerminated t) {
			// this is their way of exiting on error and not processing anything more
			// makes their code cleaner, though this gives me a feeling of exception-based control/flow :(
		}
	}

	@Override
	protected void onQuit(String sourceNick, String sourceLogin, String sourceHostname, String reason) {

		if (originalName.equals(sourceNick)) {
			changeNick(originalName);
		}
	}

	@Override
	protected void onNickChange(String oldNick, String login, String hostname, String newNick) {

		if (originalName.equals(oldNick) && accountPassword != null) {

			aggressiveTakeBackTimer.schedule(new TimerTask() {

				private int sequence;

				@Override
				public void run() {
					switch (sequence) {
					case 0:
						sendMessage("NickServ", "identify " + originalName + " " + accountPassword);
						break;
					case 1:
						sendMessage("NickServ", "release " + originalName);
						break;
					case 2:
						changeNick(originalName);
						cancel();
					}
					sequence++;
				}
			}, 3000, 3000);
		}
	}

	@Override
	protected void onDisconnect() {

		saveIndex(indexSynched);
		dispose();
	}
}
