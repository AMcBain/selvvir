package selvvir;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import selvvir.RuntimeCaniuse.Feature;

import com.zwitserloot.json.JSON;

/**
 * Quick Command Impls
 */
public class SelvvirCommands {

	public static class Set extends AbstractSelvvirCommand {

		private static final String REGEX_PATTERN = "^s/(?:\\\\/|[^/])*/(?:\\\\/|[^/])*/[igm]*$";
		private static final Pattern REGEX_PARTS = Pattern.compile("(?<![\\\\])/((?:\\\\/|[^/])*)");

		public Set() {
			super(
					"<name> = <content>",
					"Add or update a factoid", //
					"r",
					"regexp: enables regexp mode; <content> must be a sed-style (s///) substitution literal, conforming to Java regular expression syntax", //
					"a", "alias: alias mode; create an alias to an existing factoid: <content> becomes a factoid name instead", //
					"!", "force: this flag must be set to overwrite an existing factoid; otherwise a warning will be issued");
			setNotes("flags 'r' and 'a' are mutually exclusive");
			setAdminOnly();
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {

			boolean force = flags.contains("!");
			boolean alias = flags.contains("a");
			boolean regex = flags.contains("r");

			int eq = text.indexOf(" = ");
			if (eq == -1 || eq + 3 == text.length()) {

				sendError(bot, sender, SelvvirResponses.MISSING_ARGUMENTS);
			}

			String key = text.substring(0, eq).trim();
			String value = text.substring(eq + 3).trim();

			if (!regex && !alias && value.startsWith(Selvvir.ALIAS_PREFIX)) {

				sendError(bot, sender, SelvvirResponses.FACTOID_NO_ALIAS);
			}

			String oldVal = bot.getFactoid(alias ? value : key);
			boolean exists = (oldVal != null);

			if (alias && !exists) {

				sendError(bot, sender, SelvvirResponses.ALIAS_TARGET_NO_EXISTS, value);
			}

			if (alias && key.equals(value)) {

				sendError(bot, sender, SelvvirResponses.ALIAS_CIRCULAR);
			}

			if (!regex && ((!alias && exists) || (alias && bot.getFactoid(key) != null)) && !force) {

				sendError(bot, sender, SelvvirResponses.FACTOID_EXISTS, key);
			}

			if (alias && exists && oldVal.startsWith(Selvvir.ALIAS_PREFIX)) {

				value = bot.getAliasTarget(oldVal);
			}

			if (alias && bot.setFactoid(key, Selvvir.ALIAS_PREFIX + value)) {

				bot.sendNotice(target, SelvvirResponses.ALIAS_ADDED, key, value);
			}
			else if (!alias && regex) {

				if (!exists) {
					sendError(bot, sender, SelvvirResponses.REGEX_NO_TARGET, key);
				}

				if (!value.matches(REGEX_PATTERN)) {

					sendError(bot, sender, SelvvirResponses.REGEX_INVALID_FMT);
				}
				Matcher matcher = REGEX_PARTS.matcher(value);

				matcher.find();
				String find = matcher.group(1);

				matcher.find();
				String replace = matcher.group(1);

				matcher.find();
				flags = matcher.group(1);

				int rflags = 0;
				if (flags.contains("i")) {
					rflags |= Pattern.CASE_INSENSITIVE;
				}

				// Not the right one? (not what the JS regex does?)
				if (flags.contains("g")) {
					rflags |= Pattern.DOTALL;
				}

				if (flags.contains("m")) {
					rflags |= Pattern.MULTILINE;
				}

				try {
					Pattern pattern = Pattern.compile(find, rflags);
					value = pattern.matcher(bot.getFactoid(key)).replaceAll(replace);
					bot.setFactoid(key, value);
				}
				catch (PatternSyntaxException e) {

					sendError(bot, sender, SelvvirResponses.REGEX_INVALID);
				}

				bot.sendMessage(channel, SelvvirResponses.REGEX_UPDATE, key, value);
			}
			else if (!alias && bot.setFactoid(key, value)) {

				bot.sendNotice(target, exists ? SelvvirResponses.FACTOID_UPDATED : SelvvirResponses.FACTOID_ADDED, key);
			}
			else {
				sendError(bot, sender, SelvvirResponses.FACTOID_FAILED, "set", key);
			}
		}
	}

	public static class Del extends AbstractSelvvirCommand {

		public Del() {
			super("<factoid-name>", "Delete a factoid", //
					"!", "force: enables deletion of factoids that have aliases leading to it; will also delete all aliases");
			setAdminOnly();
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String factoid) {

			if (factoid.isEmpty()) {

				sendError(bot, sender, SelvvirResponses.MISSING_ARGUMENTS);
			}

			String value = bot.getFactoid(factoid);
			if (value == null) {
				sendError(bot, sender, SelvvirResponses.FACTOID_NOT_EXISTS);
			}

			List<JSON> aliases = bot.getAliases(factoid);
			if (!flags.contains("!") && aliases.size() > 0) {

				sendError(bot, sender, SelvvirResponses.ALIAS_REFERENCED, factoid);
			}
			else {
				for (JSON alias : aliases) {

					bot.deleteFactoid(alias.asString());
				}
			}

			if (bot.deleteFactoid(factoid)) {

				if (value != null && value.startsWith(Selvvir.ALIAS_PREFIX)) {

					bot.sendNotice(target, SelvvirResponses.ALIAS_DELETED, factoid, bot.getAliasTarget(value));
				}
				else {
					SelvvirResponses response = (aliases.size() > 0 ? SelvvirResponses.ALIAS_DELETED_ALL : SelvvirResponses.FACTOID_DELETED);
					bot.sendNotice(target, response, factoid, value);
				}
			}
			else {
				sendError(bot, sender, SelvvirResponses.FACTOID_FAILED, "delete", factoid);
			}
		}
	}

	// Used by Commands, and Info
	private static final String BULLET = " • ";

	public static class Commands extends AbstractSelvvirCommand {

		public Commands() {
			super("", "Display a list of special commands I support", //
					"v", "verbose: also lists the description for each command");
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {

			if (flags.contains("v")) {
				for (AbstractSelvvirCommand command : bot.commands()) {

					SelvvirResponses response = (command.isAdminOnly() ? SelvvirResponses.COMMAND_INFO_ADMIN
							: SelvvirResponses.COMMAND_INFO);

					bot.sendNotice(target, response, command.name(), command.info());
				}
			}
			else {
				StringBuilder commands = new StringBuilder();

				for (AbstractSelvvirCommand command : bot.commands()) {

					commands.append(command.name());
					commands.append(BULLET);
				}

				bot.sendNotice(target, commands.toString().replaceAll(BULLET + "$", ""));
			}
		}
	}

	public static class Info extends AbstractSelvvirCommand {

		public Info() {
			super("<factoid-name>", "Display information about a factoid");
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String factoid) {

			if (factoid.isEmpty()) {

				sendError(bot, sender, SelvvirResponses.INFO_NO_FACTOID_NAME);
			}

			String value = bot.getFactoid(factoid);
			if (value == null) {

				sendError(bot, sender, SelvvirResponses.INFO_NO_FACTOID_VALUE, factoid);
			}

			if (value.startsWith(Selvvir.ALIAS_PREFIX)) {

				String initial = factoid;
				factoid = bot.getAliasTarget(value);
				value = bot.getFactoid(factoid);

				bot.sendNotice(target, SelvvirResponses.FACTOID_IS_ALIAS, initial, factoid);
			}
			else {
				bot.sendNotice(target, SelvvirResponses.FACTOID_NOT_ALIAS, factoid);
			}

			StringBuilder aliases = new StringBuilder();
			List<JSON> list = bot.getAliases(factoid);

			int count = 0;
			for (JSON alias : list) {

				aliases.append(alias.asString());
				aliases.append(BULLET);

				if (++count == 20) {
					break;
				}
			}

			if (list.size() > 20) {

				aliases.append("…");
			}

			if (list.isEmpty()) {
				bot.sendNotice(target, SelvvirResponses.FACTOID_NO_ALIASES, factoid);
			}
			else {
				bot.sendNotice(target, SelvvirResponses.FACTOID_ALIASES, factoid, aliases.toString().replaceAll(BULLET + "$", ""));
			}
			bot.sendNotice(target, SelvvirResponses.FACTOID_INFO, factoid, value);
		}
	}

	// If you give it both flags 'o' and 'a', rivvles says it found no results (rather than telling you that you need to pick one or the
	// other)
	// If you give it both 'n' and 'c' as flags it seems to behave the same as if neither were present
	// (Note: Not doing! That's silly; behaving same as having both 'a' and 'o')
	public static class Search extends AbstractSelvvirCommand {

		public Search() {
			super("<text>", "Search the factoids store", //
					"o", "original: only search 'original' factoids; that is, factoids that are not aliases", //
					"a", "aliases: only search factoid aliases", //
					"n", "names: only search factoid names", //
					"c", "content: only search factoid content");

			setNotes("flags 'o' and 'a' are mutually exclusive", //
					"flags 'n' and 'c' are mutually exclusive");
		}

		private static final int MAX_RESULTS = 20;

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {

			if (text.isEmpty()) {
				sendError(bot, sender, SelvvirResponses.SEARCH_NO_TERM);
			}

			boolean original = flags.contains("o");
			boolean aliases = flags.contains("a");
			boolean names = flags.contains("n");
			boolean content = flags.contains("c");
			boolean all = !aliases && !original;
			boolean both = !names && !content;

			// Technically the way this is implemented below they're not. However, it seems that it might be better to warn the user
			// that what they've asked for might not return what they may have wanted and so they should change the flags they use.
			if ((original && aliases) || (names && content)) {
				bot.sendNotice(target, SelvvirResponses.SEARCH_NO_RESULTS, text);
				return;
			}

			List<String> found = new ArrayList<String>();

			for (String factoid : bot.getFactoids()) {

				boolean alias = factoid.startsWith(Selvvir.ALIAS_PREFIX);

				if (all || (aliases && alias) || (original && !alias)) {

					if ((both || names) && factoid.contains(text)) {
						found.add(factoid);
					}
					else if ((both || content) && bot.getFactoid(factoid).contains(text)) {
						found.add(factoid);
					}
				}
			}

			if (found.size() > 0) {
				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < found.size() && i < MAX_RESULTS; i++) {

					builder.append(found.get(i));

					if (i < found.size() - 1 && i < MAX_RESULTS - 1) {
						builder.append(BULLET);
					}
				}

				if (found.size() <= 20) {
					bot.sendNotice(target, SelvvirResponses.SEARCH_RESULTS, found.size() + "", text, builder.toString());
				}
				else {
					bot.sendNotice(target, SelvvirResponses.SEARCH_RESULTS_MANY, found.size() + "", text, MAX_RESULTS + "",
							builder.toString());
				}
			}
			else {
				bot.sendNotice(target, SelvvirResponses.SEARCH_NO_RESULTS, text);
			}
		}
	}

	public static class V extends AbstractSelvvirCommand {

		private static Pattern SCHEME = Pattern.compile("^[a-zA-Z][\\w+.-]*://");
		private static Pattern CSS_DOC = Pattern.compile("\\.(?:css|css\\?\\.*|css#\\.*)$");

		public V() {
			super("[<url>]", "Check a resource with the W3C markup and CSS validators.");

			setNotes("If no URL is given, the last URL seen is checked");
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {
			String url = text.trim();

			try {
				if (url.isEmpty()) {
					url = bot.getLastUrl();
				}
				if (url == null) {
					sendError(bot, sender, SelvvirResponses.VALIDATION_NO_URL);
				}
				String surl = truncate(url);

				if (!SCHEME.matcher(url).find())
				{
					url = "http://" + url;
				}

				String request;
				JSON results;
				String shortUrl;

				if (!CSS_DOC.matcher(url).find())
				{
//					request = "https://validator.w3.org/nu/?doc=" + URLEncoder.encode(url, "utf-8");
					request = "https://validator.nu/?doc=" + URLEncoder.encode(url, "utf-8");
					results = getResults(request + "&out=json&showsource=yes", null);

					if (results == null) {
						sendError(bot, sender, SelvvirResponses.VALIDATION_MARKUP_ERROR, surl);
					}

					int warnings = 0, errors = 0;
					for (JSON message : results.get("messages").asList()) {

						String type = message.get("type").asString();
						JSON subtype = message.get("subType");

						if ("error".equals(type)) {
							errors++;
						}
						else if ("info".equals(type) && subtype.exists() && "warning".equals(subtype.asString())) {
							warnings++;
						}
					}

					JSON encodingNode = results.get("source").get("encoding");
					String encoding = (encodingNode.exists() ? encodingNode.asString() : "[indeterminate]");
					shortUrl = shorten(request, bot, sender, SelvvirResponses.VALIDATION_MARKUP_ERROR, surl);

					bot.sendMessage(channel, SelvvirResponses.VALIDATION_MARKUP, target, surl, errors + "", warnings + "", encoding, shortUrl);
				}

				// Do it all again but for CSS
				request = "http://jigsaw.w3.org/css-validator/validator?uri=" + URLEncoder.encode(url, "utf-8") + "&profile=css3";
				results = getResults(request + "&output=json", null);

				if (results == null) {
					sendError(bot, sender, SelvvirResponses.VALIDATION_CSS_ERROR, surl);
				}

				results = results.get("cssvalidation");
				String level = results.get("csslevel").asString();
				JSON counts = results.get("result");

				shortUrl = shorten(request, bot, sender, SelvvirResponses.VALIDATION_CSS_ERROR, surl);

				bot.sendMessage(channel, SelvvirResponses.VALIDATION_CSS, target, surl, counts.get("errorcount").asString(),
						counts.get("warningcount").asString(), level, shortUrl);
			}
			catch (UnsupportedEncodingException e) {
				// We're hosed. UTF-8 is required to exist.
				e.printStackTrace();

				bot.sendMessage(channel, SelvvirResponses.ENCODING_ERROR, sender);
			}
		}
	}

	public static class G extends AbstractSelvvirCommand {

		public G() {
			super("<text>", "Perform a web search using Bing.");
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {
			text = text.trim();

			if (text.isEmpty()) {
				sendError(bot, sender, SelvvirResponses.GOOGLE_NO_TEXT);
			}

			try {
				String q = URLEncoder.encode(text + " -site:w3schools.com -site:tizag.com", "utf-8");
				String request = "https://api.datamarket.azure.com/Bing/SearchWeb/Web?Query=%27" + q +
						"%27&Market=%27en-US%27&$top=1&$format=JSON";
				JSON results = getResults(request, bot.getBingUser() + ":" + bot.getBingKey(), null);

				if (results == null) {
					sendError(bot, sender, SelvvirResponses.BING_ERROR);
				}

				List<JSON> items = results.get("d").get("results").asList();

				if (items.size() == 0) {
					sendError(bot, sender, SelvvirResponses.BING_NO_RESULTS, text);
				}

				String shortUrl = shorten("https://www.bing.com/search?q=" + q, bot, sender, SelvvirResponses.GOOGLE_ERROR);
				String title = unescape(items.get(0).get("Title").asString());
				String url = items.get(0).get("Url").asString();

				bot.sendMessage(channel, SelvvirResponses.BING_RESULT, target, title, url, shortUrl);
			}
			catch (UnsupportedEncodingException e) {
				// We're hosed. UTF-8 is required to exist.
				e.printStackTrace();

				bot.sendMessage(channel, SelvvirResponses.ENCODING_ERROR, sender);
			}
		}

		private static String unescape(String input) {
			return input.replace("&amp;", "&");
		}
	}

	public static class Admins extends AbstractSelvvirCommand {

		public Admins() {
			super("", "Display a list of my admins.");
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {

			java.util.Set<Object> set = bot.getAuthenticated();
			if (set == null) {
				sendError(bot, sender, SelvvirResponses.ADMINS_NONE);
			}

			StringBuilder builder = new StringBuilder();
			for (Object obj : set) {

				builder.append(", ");
				builder.append(obj);
			}

			bot.sendNotice(target, SelvvirResponses.ADMINS, builder.substring(2));
		}
	}

	public static class Short extends AbstractSelvvirCommand {

		public Short() {
			super("<url>", "Turn a (long) URL into a short one using « goo.gl »");
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {

			if (text.isEmpty()) {
				sendError(bot, sender, SelvvirResponses.GOOGLE_SHORT_NO_URL);
			}

			try {
				String surl = truncate(text);
				String response = shorten(text, bot, sender, SelvvirResponses.GOOGLE_SHORT_ERROR, surl);

				bot.sendMessage(channel, SelvvirResponses.GOOGLE_SHORT, target, surl, response);
			}
			catch (UnsupportedEncodingException e) {
				// We're hosed. UTF-8 is required to exist.
				e.printStackTrace();

				bot.sendMessage(channel, SelvvirResponses.ENCODING_ERROR, sender);
			}
		}
	}

	public static class Caniuse extends AbstractSelvvirCommand {

		private static final int MAX_RESULTS = 20;

		private RuntimeCaniuse caniuse;
		private final Map<String, String> cache = new HashMap<String, String>();

		public Caniuse() {
			super("<text>", "Search caniuse.com's browser feature support data.", //
					"e", "exact: only returns the feature whose name exactly matches the given text", //
					"d", "desktop: only returns desktop browser results", //
					"m", "mobile: only return mobile browser results", //
					"n", "near: include near-future release versions", //
					"a", "partials: count partial support as yes", //
					"p", "prefixed: count prefixed support as no");

			try {
				caniuse = new RuntimeCaniuse("caniuse/data.json");
			} catch (RuntimeException e) {
				e.printStackTrace();
				System.err.println("Caniuse data will not be available.");
			}
		}

		@Override
		public void runf(Selvvir bot, String flags, String channel, String account, String sender, String target, String text) {
			text = text.trim();

			if (caniuse == null) {
				sendError(bot, sender, SelvvirResponses.CANIUSE_NO_DATA);
			}

			if (text.isEmpty()) {
				sendError(bot, sender, SelvvirResponses.CANIUSE_NO_TEXT);
			}

			List<Feature> features = caniuse.find(text, !flags.contains("m"), !flags.contains("d"), flags.contains("n"),
					flags.contains("a"), flags.contains("p"));

			if (flags.contains("e")) {

				for (int i = 0; i < features.size(); i++) {

					if (text.toLowerCase().equals(features.get(i).key)) {

						features = features.subList(i, i + 1);
						break;
					}
				}
			}

			if (features.size() > 1) {
				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < features.size() && i < MAX_RESULTS; i++) {

					builder.append(features.get(i).key);

					if (i < features.size() - 1 && i < MAX_RESULTS - 1) {
						builder.append(BULLET);
					}
				}

				if (features.size() <= 20) {
					bot.sendNotice(sender, SelvvirResponses.CANIUSE_RESULTS, features.size() + "", text, builder.toString());
				}
				else {
					bot.sendNotice(sender, SelvvirResponses.CANIUSE_RESULTS_MANY, features.size() + "", text, MAX_RESULTS + "",
							builder.toString());
				}
			}
			else if (features.isEmpty()) {
				bot.sendNotice(sender, SelvvirResponses.CANIUSE_NO_RESULTS, text);
			}
			else {
				try {
					Feature feature = features.get(0);
					String value = feature.value;

					String specUrl = cache.get(feature.specUrl);
					String firstUrl = cache.get(feature.firstUrl);

					if (feature.specUrl != null && specUrl == null) {

						specUrl = shorten(feature.specUrl, bot, sender, SelvvirResponses.GOOGLE_ERROR);
						cache.put(feature.specUrl, specUrl);
					}
					if (feature.firstUrl != null && firstUrl == null) {

						firstUrl = shorten(feature.firstUrl, bot, sender, SelvvirResponses.GOOGLE_ERROR);
						cache.put(feature.firstUrl, firstUrl);
					}

					if (specUrl != null) {
						value += BULLET + "Spec: " + specUrl;
					}

					if (firstUrl != null) {
						value += BULLET + "Link: " + firstUrl;
					}

					bot.sendMessage(channel, SelvvirResponses.CANIUSE, target, value);
				}
				catch (UnsupportedEncodingException e) {
					// We're hosed. UTF-8 is required to exist.
					e.printStackTrace();

					bot.sendMessage(channel, SelvvirResponses.ENCODING_ERROR, sender);
				}
			}
		}
	}

}
