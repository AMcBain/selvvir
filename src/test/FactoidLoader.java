package test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import selvvir.Selvvir;

public class FactoidLoader {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Selvvir selvvir = new Selvvir("", new String[] {}, "", null, false, "", "", "", "", "");

		InputStream stream = FactoidLoader.class.getResourceAsStream("data.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

		String key, value;
		while ((key = reader.readLine()) != null) {

			value = reader.readLine();
			selvvir.setFactoid(key, value);
			reader.readLine();
		}
	}

}
